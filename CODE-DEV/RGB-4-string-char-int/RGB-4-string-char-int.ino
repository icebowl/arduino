int redPin = 2;
int greenPin = 3;
int bluePin = 4;
int red,green,blue;
void setup() {
   // put your setup code here, to run once:
   Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

String hexArray[16] = {"19C1D5","00DD00","0000CC","FFFF00","009999","FF00DD","CB6077","D28B71",
                      "F4BC87","BEB55B","7BBDA4","8AB3B5","A89BB9","BB9584","203731","FFB612"};
String hexStr;
int count = 0;
int ci;

void loop() {
  hexStr = hexArray[count];
  char buf[7];
  int dec[6];
  hexStr.toCharArray(buf,7);
  for (int i = 0; i < 6;i++){
         dec[i] = charInt(buf[i]);
    }
    red = dec[0]*10+dec[1];
    green = dec[2]*10+dec[3];
    blue = dec[4]*10+dec[5];

    analogWrite(redPin, red);
      analogWrite(greenPin, green);
      analogWrite(bluePin, blue);
      count = count + 1;
      if (count > 16) count = 0;
      delay(1000);
}

int charInt(char c){
  int hi = (int)c;
  if(hi < 65){
    hi = hi -48;}
      else{
    hi = hi - 55;
    }
  return hi;
}
