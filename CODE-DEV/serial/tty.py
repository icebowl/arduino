##############
## Script listens to serial port and writes contents into a file
##############
## requires pySerial to be installed 
import serial
import sys
import os
import time

serial_port = '/dev/ttyACM0';
baud_rate = 9600; #In arduino, Serial.begin(baud_rate)

ser = serial.Serial(serial_port, baud_rate)
while True:
	line = ser.readline();
	#line = line.decode("utf-8") #ser.readline returns a binary, convert to string
	#print(line,end="")
	linestr = str(line)
	#print(linestr)
	lineclean = linestr.replace("b","")
	lineclean = lineclean.replace("\'","")
	lineclean = lineclean.replace("\\r","")
	lineclean = lineclean.replace("\\n","")
	#print(lineclean)
	#sys.stdout.write(linestr)
	lineInt = int(lineclean)
	#print("lineInt ",lineInt)
	if (lineInt < 10):
		print("less than 30")
		cmd = "raspistill -o now.png"
		os.system(cmd)
		time.sleep(3)
		lineInt = 100000

