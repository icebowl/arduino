#include <Arduino.h>

#define DECODE_HASH             // special decoder for all protocols
#define RAW_BUFFER_LENGTH 1000  // Especially useful for unknown and probably long protocols
//#define DEBUG                 // Activate this for lots of lovely debug output from the decoders.

#include "PinDefinitionsAndMore.h"
#include <IRremote.hpp>  // include the library

void setup() {
  Serial.begin(115200);
  while (!Serial)
  Serial.println(F("START " __FILE__ " from " __DATE__ "\r\nUsing library version " VERSION_IRREMOTE));
  IrReceiver.begin(IR_RECEIVE_PIN, ENABLE_LED_FEEDBACK);
  Serial.println(F("Ready to receive unknown IR signals at pin " STR(IR_RECEIVE_PIN) " and decode it with hash decoder."));
}
unsigned int data;
void loop() {
  if (IrReceiver.available()) {
    IrReceiver.initDecodedIRData();  // is required, if we do not call decode();
    IrReceiver.decodeHash();
    //IrReceiver.printIRResultRawFormatted(&Serial, true);
    IrReceiver.resume();  // Early enable receiving of the next IR frame

    //IrReceiver.printIRResultShort(&Serial);
    //Serial.println();

    /*
         * Finally, check the received data and perform actions according to the received command
         */
    
    data = IrReceiver.decodedIRData.decodedRawData;
    //Serial.print(" * * * ");
    //Serial.println(data);
    //Serial.println(" * * * ");
    // UP 33556 RIGHT 47070  LEFT 55916  DOWN 24860
    if (data == 33556)Serial.print(" UP ");
     if (data == 24860)Serial.print(" DOWN ");
      if (data == 47070 )Serial.print(" RIGHT ");
       if (data == 55916 )Serial.print(" LEFT ");
   // if (IrReceiver.decodedIRData.decodedRawData == 0x4F7BE2FB) {
      // do something
   // } else if (IrReceiver.decodedIRData.decodedRawData == 0x97483BFB) {
      // do something else
   // }
  }
}
