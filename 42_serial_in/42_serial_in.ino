// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library
// Modified by CraigColeman  for an 8x8 neopixel
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
int clearPix ();
int cwc ();
int char2Int(int ichr);
String instring, instr, teststr;
String hexstr,redHex, greenHex, blueHex,whiteHex;
char c16,c1;
int i100, i10,i1;
int count = 0;
int redi, greeni, bluei,whitei = 0;



// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            6

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      64

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
//  8x8 array

int i = 0, j = 0;


//
int delayval = 50; // delay for half a second

void setup() {
  Serial.begin(9600);      //ser up Serial.*
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  // End of trinket special code

  pixels.begin(); // This initializes the NeoPixel library.
}
int once = 0;
void loop() {
  if (Serial.available() > 0 && once > 0)
  {
      instring = Serial.readString();
      //Serial.println( instring, String);
      Serial.println("in serial");
      instr = instring.substring(0, 8);
       whiteHex= instr.substring(6,8);
        c16 = whiteHex[0];c1 = whiteHex[1];
        i10 = char2Int(c16)*16;
        i1 = char2Int(c1);
        whitei = i10 + i1;

      
  }else{
    instring = "00112200";
    once = 1;
  }
   
cwc(whitei);  //delay(2000);
 whitei = whitei + 20;
  //if(whitei > 255) whitei = 0;
   Serial.print(" whitei ");
  Serial.println(whitei);
}

int clearPix (){
  for(int i=0;i<NUMPIXELS;i++){
      pixels.setPixelColor(i, pixels.Color(0,0,0)); 
    }
    pixels.show();
  }
char pixval = '0';

int cwc(int n){
//ser base color
int r,c;//row and column
int red = 0, green = 0, blue = 0;

String ms[8] = { 
                  "40040220",
                  "40042002",
                  "40040002",
                  "40040220",
                  "44440200",
                  "00042000",
                  "00042000",
                  "00042222" };

  i = 0;// set neo-pixel number to zero
  int val;
//  loop that increments x,y and i
  for (r = 0; r < 8; r++){
    for (c = 0; c < 8; c++){
      pixval = ms[r][c];
      // set the color for red green and blue
      val = char2Int(pixval);
     // Serial.print(" ");
     // Serial.print(val);
      switch ( val ) {
          case 0:
            red = 0; green = 0; blue =0;
          break;
          case 1:
            red = 0; green = 30; blue =0;
          break;
           case 2:
          red = 0; green = 0; blue =30;
          break;
            case 3:
          red = 30; green = 30; blue =0;
          break;
            case 4:
          red = 0; green = 33; blue =30;
          break;
           case 5:
          red = 0; green = 33; blue =30;
          break;
           case 6:
          red = 0; green = 33; blue =30;
          break;
          default:
          red = 33; green = 0; blue =30;
          break;
}

      
      // end of set the color
      pixels.setPixelColor(i, pixels.Color(red,green,blue));
       pixels.setBrightness(n);
      pixels.show(); 
      i++;
     
     //Serial.print(c[x][y]);
    }//end y
    
}//end y and end of loop that increments x,y and i
}

int char2Int(int ichr){
  int dec;
  if(ichr >= 97 && ichr <= 102){
    dec  =  ichr -87;
  }else{
    dec = ichr - 48;
  }
  
  return dec;
}
