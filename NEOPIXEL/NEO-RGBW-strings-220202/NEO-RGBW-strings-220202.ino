// RGBW (Red Green Blue White Neo-Pixel starter code
// 16 LEDS  increment
// CW Coleman 220202

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6
#define NUM_LEDS 16
#define BRIGHTNESS 50

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);
//globals


void setup() {
  Serial.begin(115200);
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

// Initialize some variables for the void loop()


String hexArray[16] = {"FFFFFF","FF0000","0000CC","FFFF00","009999","FF00DD","CB6077","D28B71",
                      "F4BC87","BEB55B","7BBDA4","8AB3B5","A89BB9","BB9584","203731","FFB612"};
String hexStr;
int count = 0;
int ci;

void loop() {

  for (int i = 0;i < 16;i++){
    setColor(hexArray[i]);
   }
}//end loop

// void setColor
int led;  int redi,greeni,bluei,whitei;
void setColor(String rgb){
 char buf[7];
  int dec[6];

  rgb.toCharArray(buf,7);
  for (int i = 0; i < 6;i++){
         dec[i] = charInt(buf[i]);
    }
    redi = dec[0]*10+dec[1];
    greeni = dec[2]*10+dec[3];
    bluei = dec[4]*10+dec[5];
    // set colors 
    whitei = 0;
    for (led = 0; led < 16 ; led++){
        strip.setPixelColor(led, redi, greeni , bluei, whitei);
        strip.show();
        delay(20);
    }
 
  
     delay(2000);

    }


int charInt(char c){
  int hi = (int)c;
  if(hi < 65){
    hi = hi -48;}
      else{
    hi = hi - 55;
    }
  return hi;
}


               
