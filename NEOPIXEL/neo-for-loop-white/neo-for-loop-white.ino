// RGBW (Red Green Blue White Neo-Pixel starter code
// 16 LEDS  increment
// CW Coleman 170413

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6

#define NUM_LEDS 16

#define BRIGHTNESS 50

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

// Initialize some variables for the void loop()
int red = 0, green= 0, blue = 0, white = 0;
int wait = 500;
int led = 0;
int i;

void loop() {
  red = 13; green = 127; blue = 13;
  for ( led = 0; led < 16; led++){  
    strip.setPixelColor(led, red, green , blue, 50);
    strip.setBrightness(white);
  }//end of for loop
    strip.show();
    delay(3000);
    white = white + 10;
    if (white > 50) white = 0;
    if (white  < 0 ) white = 50;
}
