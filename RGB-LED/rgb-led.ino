// pin globals
int redPin2 = 7;
int greenPin2 = 6;
int bluePin2 = 5;

int redPin1 = 4;
int greenPin1 = 3;
int bluePin1 = 2;

// setup function
void setup() {
  Serial.begin(9600);
  pinMode(redPin1, OUTPUT);
  pinMode(greenPin1, OUTPUT);
  pinMode(bluePin1, OUTPUT);
  pinMode(redPin2, OUTPUT);
  pinMode(greenPin2, OUTPUT);
  pinMode(bluePin2, OUTPUT);
}

// Gloabls
int count = 0;
int redValue = 0; int greenValue = 0; int blueValue = 0;
/////////////////////////

void loop() {
  
    redValue = redValue + 1;
    greenValue = greenValue - 1;
    blueValue = blueValue + 1;
    if (redValue > 255 ) {redValue = 0;}
    if (greenValue > 255 ){greenValue = 0;}
    if (blueValue > 255 ) {blueValue = 0;}
    if (redValue < 0 ) {redValue = 255;}
    if (greenValue < 0 ) {greenValue = 255;}
    if (blueValue < 0) {blueValue = 255;}
    //delay(1000);
    analogWrite(redPin1, redValue);
    analogWrite(greenPin1, greenValue);
    analogWrite(bluePin1, blueValue);
    analogWrite(redPin2, 255 - redValue);
    analogWrite(greenPin2, 255 - greenValue);
    analogWrite(bluePin2, 255 - blueValue);
    Serial.print(redValue);Serial.print(" ");
    Serial.print(greenValue);Serial.print(" ");
    Serial.println(blueValue);
} //end loop
