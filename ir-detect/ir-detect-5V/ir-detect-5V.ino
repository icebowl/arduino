//https://makersportal.com/blog/2018/1/17/infrared-obstacle-detector-for-less-than-1
//https://makersportal.com/shop
//Photodiode to digital pin 2 optional
int led=13;                   //LED to digital pin 7  
int senRead=0;                 //Readings from sensor to analog pin 0  
int limit = 1000;            // play with system to find this limit (10K ohm resistor used)
void setup()    
 {  
  pinMode(led,OUTPUT);  
  digitalWrite(led,LOW);      // LED initially off  
  Serial.begin(9600);         
 }  

int val = 0;
void loop()  
 {  
  val=analogRead(senRead);  //photodiode reading
  Serial.println(val);      
  if(val <= limit)              //If obstacle is nearer than the Threshold range  
  {  
   digitalWrite(led,LOW);     // LED will be ON
   delay(20);  
  }  
  else if(val > limit)          //If obstacle is not in Threshold range  
  {  
   digitalWrite(led,HIGH);      //LED is OFF
   delay(20);  
  }  
 }  
