/*
    Complete Guide for Ultrasonic Sensor HC-SR04
    Ultrasonic sensor Pins:
        VCC: +5VDC
        Trig : Trigger (INPUT) - Pin11
        Echo: Echo (OUTPUT) - Pin 12
        GND: GND
 */
 
 /*
The RGB LED uses pin 4,3,2 for RGB
Notice the void setColor(int redValue, int greenValue, int blueValue)
function.  "void" indicates the function does not return a value.
Also notice the  "if" logic related to centimeter proximity.
&& is a logical AND .  Both conditions need to be true.
if(cm < 60 && cm > 40 )  {setColor(255, 0, 0); delay(500);} // Red Color
  if(cm <= 40)  {setColor(0, 0, 255); delay(500);} // Blue Color

 */
int trigPin = 11;    // Trigger
int echoPin = 12;    // Echo
int redPin= 4;
int greenPin = 3;
int bluePin = 2;
long duration, cm, inches;
 
void setup() {
  //Serial Port begin
  Serial.begin (9600);
  //Define inputs and outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}
 
void loop() {
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  inches = (duration/2) / 74;   // Divide by 74 or multiply by 0.0135
 
  Serial.println(cm);
  setColor(0, 255, 0); // Green Color
  if(cm < 60 && cm > 40 )  {setColor(255, 0, 0); delay(500);} // Red Color
  if(cm <= 40)  {setColor(0, 0, 255); delay(500);} // Blue Color
  //setColor(0, 0, 255); // Blue Color 
  delay(125);
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
